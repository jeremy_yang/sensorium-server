#sensorium-server
sensorium-server is a broker tool based on `socket.io` and [serialport](https://www.npmjs.com/package/serialport). With this tool, you can connect HTML pages easily to your computer USB serialport and debug your protocol communication with the hardware.


# Change log
- sensorium-server@0.1.1 is based on serialport@6.0.4, this 6.0.4 version has fixed some bugs in windows

- sensorium-server@0.1.0 is based on serialport@6.0.0

# Install
You can install it globally like this as **recommendation**.
```
npm install sensorium-server -g
```
or install locally in your project,
```
npm install sensorium-server -save-dev
```

some permission error may be resolved by:
```
npm config set unsafe-perm true
```
*if you update your node version, probably sensorium-server would not work. You just need to update your `ss` by `npm install sensorium-server -g` to rebuild it.*

# Usage
Firstly, make sure you have initialized your project. Then complete the JavaScript code to set up connections between your pages and the `sensorium-server` tool just like this:
```
import io from 'socket.io-client';
let socket = io.connect('http://localhost:8800');

socket.on('sensor2web', function (data) {
    console.log(JSON.parse(data));
});

let buf = [0xf3, 0x0a, 0x55, 0xf4];
socket.emit('web2sensor', { buf: JSON.stringify(buf) });
```

Secondly, open your command line and in any file directories（because it can run globally）type in
```
sensorium-server
```
or just simply like this,
```
ss
```
** You may need connect a USB line to your computer before, or it would fail to start*

when comes the start-up log, it says the connection has been established！
```
>>> ss
Starting up sensorium-server, serving on port 8800
  http://127.0.0.1:8800
Hit CTRL + C to stop the server

Defualtly ss connets to the [0] ports as follows:
[ 0 ] : /dev/tty.wchusbserial1420  √
[ 1 ] : /dev/tty.Makeblock-ELETSPP
[ 2 ] : /dev/tty.Bluetooth-Incoming-Port
If there comes an error or no response, you should try to connect to the other port:
 ss -s 1
```