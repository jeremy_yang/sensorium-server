var colors = require('colors/safe');
var HttpServer = require('./core');
var serialConnect = require('./serialConnect');
var Emitter = require('./emitter');
var socket = require('socket.io');

const loggerinfo = console.log;

exports.createServer = function(options) {
  return new Server(options);
};

function Server(options) {
  options = options || {};
  var httpServer = new HttpServer();

  //创建服务器
  this.app = httpServer.createServer(options);

  var io = socket(this.app);

  io.on('connection', function(socket) {
    // 监听来自串口的数据
    Emitter.on('receive', (buff) => {
      // 向浏览器发送数据
      socket.emit('sensor2web', JSON.stringify(buff));
    });

    // 监听来自浏览器的数据
    socket.on('web2sensor', function(data) {
      loggerinfo(
        '[%s] "%s %s',
        new Date().getTime(),
        colors.yellow('web -> sensor'),
        colors.yellow(data.buf)
      );
      let buf = JSON.parse(data.buf);
      // 转发给串口
      Emitter.emit('send', buf.map((val) => Number(val)));
    });
  });

  //建立串口连接
  serialConnect(options);
}

Server.prototype.listen = function() {
  this.app.listen.apply(this.app, arguments);
};

Server.prototype.close = function() {
  return this.app.close();
};