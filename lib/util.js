exports.arrayFromArrayBuffer = function (buffer) {
  var dataView = new Uint8Array(buffer);
  var result = [];
  for (var i = 0; i < dataView.length; i++) {
    result.push(dataView[i]);
  }
  return result;
}