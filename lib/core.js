var http = require('http');
var url = require('url');
var querystring = require('querystring');
var Emitter = require('./emitter');

function HttpServer () {

}

HttpServer.prototype.createServer = function (options) {
	function requestHandler(req, res, error) {
		logReq(req, res, error);
		// console.log('xxxxxxx', req.headers['content-type']);
		res.setHeader('Access-Control-Allow-Origin', '*');
 		res.setHeader('Access-Control-Allow-Headers',
 			'Origin, X-Requested-With, Content-Type, Accept, Range');
 		if (options.corsHeaders) {
        var headers =options.corsHeaders.split(/\s*,\s*/).forEach( h => ', '+h).join('');
        res.setHeader('Access-Control-Allow-Headers', headers);
    }
    //Content-Type
    res.setHeader("Content-Type", "application/json; charset=utf-8");
    if(error){
    	this.errorHandler(arguments);
    }
	}
	return http.createServer(requestHandler);
};

HttpServer.prototype.errorHandler = function (err, req, res) {
  if (err) {
    res.writeHead(err.status || 500, err.headers || { "Content-Type": "text/plain" });
    res.end(err.message + "\n");
    return;
  }
  res.writeHead(404, {"Content-Type": "text/plain"});
  res.end("null");
};


module.exports = HttpServer;