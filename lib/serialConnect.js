var colors = require('colors/safe');
var SerialPort = require('serialport');
var Emitter = require('./emitter');
var arrayFromArrayBuffer = require('./util').arrayFromArrayBuffer;

var logInfo = console.log;

function serialConnect(options) {
  let cnt = typeof options.cntPort !== 'number'? 0: parseInt(options.cntPort);
  SerialPort.list().then((ports) => {
    if(!ports.length) {
      logInfo(colors.red(`there's no serial ports available`));
      return;
    }
    let paths = ports.map(val => val.comName).reverse();
    let mySelect = cnt < paths.length && cnt >= 0? cnt:0;

    let pathList = paths.map((val, i) =>
      `[ ${colors.cyan(i)} ] : ${val}  ${mySelect===i?colors.green('√'):''}`
    );
    logInfo(colors.yellow('Defualtly ss connets to the [0] ports as follows:'));
    logInfo(pathList.join('\n'));
    logInfo(colors.yellow(`If there comes an error or no response, you should try to connect to the other port:`));
    logInfo(colors.cyan('  ss -s 1\n'));

    let cntPort = new SerialPort(paths[mySelect], {baudRate: 115200});
    cntPort.on('open', function(msg) {

      cntPort.on('data', function(buff) {
        let logbuf = arrayFromArrayBuffer(buff);
        logInfo(
          '[%s] "%s [%s]',
          new Date().getTime(),
          colors.green('sensor -> web'),
          colors.green(logbuf.join(','))
        );
        Emitter.emit('receive', buff);
      });

      Emitter.on('send', function(buf){
        cntPort.write(buf);
      })
    });

    cntPort.on('error', function(err) {
      console.log('Error: ', err.message);
    });
  });

}

module.exports = serialConnect;
