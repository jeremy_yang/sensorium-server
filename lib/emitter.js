// 模块只会被执行一次
var EventEmitter = new require('events').EventEmitter;
var Emitter = new EventEmitter();

module.exports = Emitter;