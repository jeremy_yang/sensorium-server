module.exports = function(){
	console.log([
		'usage: sensorium-server [path] [options]',
		'',
		'options:',
		'  -p           Port to use [8800]',
		'  -a           Address to use [0.0.0.0]',
		'  -s           Select serial port to connect [1]',
		'  -d           Show directory listings [true]',
		'  -req         Enable request log messages [true]',
		'  -res         Enable response log messages [true]',
		'  -i           Display autoIndex [true]',
		'  --cors[=headers]   Enable CORS via the "Access-Control-Allow-Origin" header',
		'                     Optionally provide CORS headers list separated by commas',
		'  -o [path]    Open browser window after starting the server',
		'  -c           Cache time (max-age) in seconds [3600], e.g. -c10 for 10 seconds.',
		'               To disable caching, use -c-1.',
		'  -U --utc     Use UTC time format in log messages.',
		'',
		'  -h --help    Print this list and exit.'
	].join('\n'));
}