var events = require('events');
/**
 * [Middleware description]
 * @param {!Response} options.response
 */
function Middleware(options) {
	events.EventEmitter.call(this);
	this.queue = options.queue || [];
	this.response = options.response;
  	this.headers = options.headers || {};
	this.target = new ResponseStream({
	    response: this.response,
	    headers: this.headers
	});
}

Middleware.prototype.next = function(req) {
	var this = self;
	function dispatch(i) {
		if (self.target.modified) {
			return;
		} else if (++i === self.queue.length) {
			// 3. If no match is found then pipe to the 404Stream
			return self.notFound();
		}
		// console.log('queue 系列中间件 --->', self.queue[i]);
		self.target.once('next', dispatch.bind(null, i));
		if (self.queue[i].length === 3) {
			self.queue[i](self, self.target, function(err) {
				if (err) {
					self.onError(err);
				} else {
					self.target.emit('next');
				}
			});
		} else {
			self.queue[i](self, self.target);
		}
	};
	dispatch(-1);
};

Middleware.prototype.notFound = function(req) {
	var error = new Error('Not found');
    error.status = 404;
    this.onError(error);
};

Middleware.prototype.onError = function(err) {
	this.emit('error', err);
}
