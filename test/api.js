var SerialPort = require('serialport');

SerialPort.list().then(ports => {
  console.log('ports', ports.map(val => val.comName).reverse());
  var port = new SerialPort('/dev/tty.wchusbserial1420', {
    baudRate: 57600
  })

  port.on('open', (data) => {
    console.log('open', data);
    port.get((msg)=>{
      console.log('port.get', msg);
    })
  })

  port.on('data', (data) => {
    console.log('data', data);
  })

  port.on('error', (data) => {
    console.log('error', data);
  })

  port.on('close', (data) => {
    console.log('close', data);
  })
});


