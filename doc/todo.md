##目标:
1、处理请求：作为一个 socket-io 服务，连接 web 端和 serialPort I/O 模块
2、打印日志：打印请求日志、响应数据日志
3、尝试连接更多的 I/O 模块，比如 wifi、Ble

##具体节点目标：
1、√ 在本项目根目录开启 sensorium-server
2、√ 客户端 8080 跨域请求到 8800 服务端口，8800 服务端返回跨域请求数据
3、√ 8800 服务端向 serialPort 发送数据
4、√ 8800 服务端接收自 serialPort 的数据
5、√ 完成目标


##目前已预研：
1、union 库
2、http-server 库
3、http-proxy 库
上述都不能解决问题，但提供了一定的思路参考。

##解决 TODO：
1、研究 http 模块，requestStream、responseStream 等流对象
2、node 中间件机制
3、shell 脚本及执行

4、急切需要了解参数、body参数、dataType、content-type这些鬼东西的具体该怎么处理了——这个严重阻碍了进程
5、不用 express 坑也会很多

###新增问题
1、√ windows 会出现多个 USB 串口，连接后无法得知那个一是需要的
2、开启了多个客户端页面，会出现 port 被占用的错误报告
3、更新了 node 版本，需要重新下载 ss， 以便能够重新编译其中的 serialport 模块。更具体的操作可能非常麻烦：
 - 先卸载 ss， 再 npm install 它。
 - 如果卸载失败，很可能因为 npm 也被更新了——5.x 版本导致卸载也失败了。这个时候请使用 cnpm 卸载和安装ss ——前提是你有 cnpm




